//
//  TRLConst.h
//  GWRNaviBar
//
//  Created by Loi Wu on 7/16/15.
//  Copyright (c) 2015 Loi Wu. All rights reserved.
//

#define kStatusBarHeight                20.0

#define kNaviBarBackButtonWidth         44
#define kNaviBarCenterWidth             TBRightAlignWidthWithScreenSize(140)
#define kNaviBarCenterMaxWidth          TBRightAlignWidthWithScreenSize(180)

#define kNaviBarRightWith               60
#define kNaviBarDefaultHeight           44.0
//naviBar 返回和right对应的字体大小  标准3,32px
#define kNaviBarFontSize                16.0
//默认返回箭头大小
#define kDefaultNaviBarBackImageSize    CGSizeMake(16, 24)

//返回字体的颜色，K 0x5f646e
#define kNaviBarBackColor               [UIColor colorWithRed:0x5f/255.0 green:0x64/255.0 blue:0x6e/255.0 alpha:1.0]
//navibar 标题颜色，L 0x3d4245
#define kNaviBarCenterTitleColor        [UIColor colorWithRed:0x3d/255.0 green:0x42/255.0 blue:0x45/255.0 alpha:1.0]
//navibar 标题字体大小，标准2 ,36px
#define kNaviBarCenterTitleFont         18
//right内容和tao的间距
#define kNaviBarRightTaoOffset          -4.0
//highlight 效果，设置0.3的透明度
#define kNaviBarHighLightAlpha          0.3

#define kDefaultSeparateLineHeight      0.5  //naviBar上面或者下面分割线高度
#define kDefaultSeparateLineColor       [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.3]

#define TRLObjectAtIndex(array,index)    (array&&(index>=0)&&(index<[array count])) ? [array objectAtIndex:index] : nil

//#define kStatusAdapterOffset (([UIDevice TBSystemVersion] > 6.1)? 0.0 : 0)//ios7.0适配
#define kStatusOffset (([[[UIDevice currentDevice] systemVersion] floatValue] > 6.1)? 20 : 0)//ios7.0适配

