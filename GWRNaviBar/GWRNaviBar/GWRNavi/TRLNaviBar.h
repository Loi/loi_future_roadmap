//
//  TRLNaviBar.h
//  GWRNaviBar
//
//  Created by Loi Wu on 7/16/15.
//  Copyright (c) 2015 Loi Wu. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^TRLStatusBarClickedBlock)();


@class TRLNavigationItem;

/**
 *  定制的导航栏类
 */
@interface TRLNaviBar : UIView

/**
*  导航栏的位置偏移
*/
@property (nonatomic, assign) UIEdgeInsets contentInsets; // Default: UIEdgeInsetsMake(6, 8, 0, 8)

/**
 *  设置导航栏显示内容
 *
 *  @param item TRLNavigationItem类型，控制显示内容
 */
- (void)setNavigationItem:(TRLNavigationItem *)item;

/**
 *  设置naviBar是否使用毛玻璃背景
 */
- (void)setBackgroundBlurViewHidden:(BOOL)isHidden;

/**
 *  设置naviBar底部分隔线是否隐藏
 */
- (void)setBottomLineHidden:(BOOL)isHidden;

/**
 *  设置允许状态栏响应点击事件
 *
 *  @param block 响应block
 */
- (void)setEnableStatusBarClicked:(TRLStatusBarClickedBlock)block;

/**
 *  设置背景毛玻璃的颜色
 *
 *  @param color 颜色
 */
- (void)setBackgroundBlurViewColor:(UIColor*)color;

/**
 *  设置bar的背景图片
 *
 *  @param image 背景图片
 */
- (void)setBarBackgroundImage:(UIImage*)image;


/**
 *  设置全局默认导航栏显示内容
 *
 *  @param item TRLNavigationItem类型，控制显示内容
 */
+ (void)setDefaultNavigationItem:(TRLNavigationItem *)item;

/**
 *  设置全局默认naviBar是否使用毛玻璃背景
 */
+ (void)setDefaultBackgroundBlurViewHidden:(BOOL)isHidden;

/**
 *  设置全局默认naviBar底部分隔线是否隐藏
 */
+ (void)setDefaultBottomLineHidden:(BOOL)isHidden;

/**
 *  设置全局默认允许状态栏响应点击事件
 *
 *  @param block 响应block
 */
+ (void)setDefaultEnableStatusBarClicked:(TRLStatusBarClickedBlock)block;

/**
 *  设置全局默认背景毛玻璃的颜色
 *
 *  @param color 颜色
 */
+ (void)setDefaultBackgroundBlurViewColor:(UIColor*)color;

/**
 *  设置全局默认bar的背景图片
 *
 *  @param image 背景图片
 */
+ (void)setDefaultBarBackgroundImage:(UIImage*)image;

@end
