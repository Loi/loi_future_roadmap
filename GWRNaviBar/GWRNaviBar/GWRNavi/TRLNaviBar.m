//
//  TRLNaviBar.m
//  GWRNaviBar
//
//  Created by Loi Wu on 7/16/15.
//  Copyright (c) 2015 Loi Wu. All rights reserved.
//

#import "TRLNaviBar.h"
#import "TRLNavigationItem.h"
//#import "TRLBlurView.h"
#import "TRLConst.h"

@interface TRLNaviBarDefaultConfig :NSObject

@property (nonatomic, strong) TRLNavigationItem *navigationItem;
@property (nonatomic, assign) BOOL backgroundBlurViewHidden;
@property (nonatomic, assign) BOOL bottomLineHidden;
@property (nonatomic, strong) TRLStatusBarClickedBlock enableStatusBarClicked;
@property (nonatomic, strong) UIColor *backgroundBlurViewColor;
@property (nonatomic, strong) UIImage *barBackgroundImage;

@end

@implementation TRLNaviBarDefaultConfig

+ (instancetype)sharedInstance{
    static TRLNaviBarDefaultConfig* sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

@end

@interface TRLNaviBar () {
    NSArray             *_items;
    NSMutableArray      *_itemViews;
//    TRLBlurView          *_backgroundImageView;
    TRLNavigationItem    *_navigationItem;
    UIView              *_bottomLineView;
}
@property(nonatomic, copy)TRLStatusBarClickedBlock statusBarClickedBlock;

@end

@implementation TRLNaviBar

+ (void)setDefaultNavigationItem:(TRLNavigationItem *)item{
    [[TRLNaviBarDefaultConfig sharedInstance] setNavigationItem:item];
}

+ (void)setDefaultBackgroundBlurViewHidden:(BOOL)isHidden{
    [[TRLNaviBarDefaultConfig sharedInstance] setBackgroundBlurViewHidden:isHidden];
}

+ (void)setDefaultBottomLineHidden:(BOOL)isHidden{
    [[TRLNaviBarDefaultConfig sharedInstance] setBottomLineHidden:isHidden];
}

+ (void)setDefaultEnableStatusBarClicked:(TRLStatusBarClickedBlock)block{
    [[TRLNaviBarDefaultConfig sharedInstance] setEnableStatusBarClicked:block];
}

+ (void)setDefaultBackgroundBlurViewColor:(UIColor*)color{
    [[TRLNaviBarDefaultConfig sharedInstance] setBackgroundBlurViewColor:color];
}

+ (void)setDefaultBarBackgroundImage:(UIImage*)image{
    [[TRLNaviBarDefaultConfig sharedInstance] setBarBackgroundImage:image];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = CGRectMake(0, kStatusBarHeight, [[UIScreen mainScreen] bounds].size.width, kNaviBarDefaultHeight);
        self.contentInsets = UIEdgeInsetsMake(10, 8, 0, 8);
        _itemViews = [[NSMutableArray alloc] initWithCapacity:0];
        self.opaque = NO;
        [self setNavigationItem:[TRLNaviBarDefaultConfig sharedInstance].navigationItem];
        [self setEnableStatusBarClicked:[TRLNaviBarDefaultConfig sharedInstance].enableStatusBarClicked];
    }
    return self;
}

#pragma mark - Public  Method
- (void)setNavigationItem:(TRLNavigationItem *)item{
    _navigationItem = item;
}

@end
