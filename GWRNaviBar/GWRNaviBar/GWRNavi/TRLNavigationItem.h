//
//  TRLNavigationItem.h
//  GWRNaviBar
//
//  Created by Loi Wu on 7/16/15.
//  Copyright (c) 2015 Loi Wu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TRLNavigationItem : NSObject

/**
 *  naviBar左侧显示内容，一般设置为默认的返回按钮，从左布局，偏移量由TBNaviBar 中的contentInsets指定。
 */
@property (nonatomic, strong)UIBarButtonItem                 *trlNavibarLeftItem;

/**
 *  naviBar右侧显示内容
 */
@property (nonatomic, strong)UIBarButtonItem                 *trlNavibarRightItem;
/**
 *  naviBar中间显示内容，会自动设置居中。
 */
@property (nonatomic, strong)UIBarButtonItem                 *trlNavibarCenterItem;

@end

