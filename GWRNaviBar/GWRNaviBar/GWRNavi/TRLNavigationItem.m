//
//  TRLNavigationItem.m
//  GWRNaviBar
//
//  Created by Loi Wu on 7/16/15.
//  Copyright (c) 2015 Loi Wu. All rights reserved.
//

#import "TRLNavigationItem.h"

@implementation TRLNavigationItem

- (void)setTRLNavibarLeftItem:(UIBarButtonItem *)trlNavibarLeftItem{
    if (_trlNavibarLeftItem != trlNavibarLeftItem) {
        _trlNavibarLeftItem = trlNavibarLeftItem;
    }
}

- (void)setTRLNavibarCenterItem:(UIBarButtonItem *)trlNavibarCenterItem{
    if (_trlNavibarCenterItem != trlNavibarCenterItem) {
        _trlNavibarCenterItem = trlNavibarCenterItem;
    }
}

- (void)setTRLNavibarRightItem:(UIBarButtonItem *)trlNavibarRightItem{
    if (_trlNavibarRightItem != trlNavibarRightItem) {
        _trlNavibarRightItem = trlNavibarRightItem;
    }
}

@end
