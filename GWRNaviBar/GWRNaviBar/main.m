//
//  main.m
//  GWRNaviBar
//
//  Created by Loi Wu on 7/16/15.
//  Copyright (c) 2015 Loi Wu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
